'use strict';

//Variables
let number = Math.trunc(Math.random()* 20) + 1;
let score = 20;
let highScore = 0;

//document.querySelector('.number').textContent = number;

function displayMessage (message) {
    document.querySelector('.message').textContent = message;
}

function displayNumber (number) {
    document.querySelector('.number').textContent = number;
}

function lenghtOfTheNumber (width){
    document.querySelector('.number').style.width = width; 
}

function scoreTextContent (score){
    document.querySelector('.score').textContent = score;
}

//check button
    document.querySelector('.check').addEventListener('click', function(){
    const guess = Number(document.querySelector('.guess').value);
    console.log(guess, typeof guess);

    //no guess
    if (!guess) {
        //document.querySelector('.message').textContent = 'No number!';
        displayMessage('No number');
    }

    //player wins
    else if (guess === number){
       displayMessage('This is the right number, congrats!');

        document.querySelector('body').style.backgroundColor = '#60b347';

        lenghtOfTheNumber('30rem');
        displayNumber(number);

    //highScore checking
        if ( score > highScore) {
            highScore = score;
            document.querySelector('.highscore').textContent = highScore;
        }
    }

    else if (guess != number) {

        if ( score > 0){
        displayMessage(guess > number ? 'Too high!' : 'Too low!');
        score--;
        scoreTextContent(score);
        }

        else {
        displayMessage('you lost!');
        }
    }
});

//again button
document.querySelector('.again').addEventListener('click', function(){
    
    score = 20;
    number = Math.trunc(Math.random()* 20) + 1;

    displayMessage('Start guessing...');
    scoreTextContent(score);
    displayNumber('?');
    lenghtOfTheNumber('15rem');

    document.querySelector('.guess').value = '';
    document.querySelector('body').style.backgroundColor = '#222';
    
});
